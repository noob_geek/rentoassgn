var async = require("async");
var request = require("request")
var cheerio = require("cheerio")
var fs = require("fs")
var sub_links
var link_set = new Set

function crawl(url, callback) {
    
    request(url, function (error, response, body) {
	  	
	  	if (!error) {
	    	var $ = cheerio.load(body)
	    	var temp=[]
	    	links=$('a')
	    	$(links).each(function(i,link){
	    		if($(link).attr('href')!=undefined){
	    			if(!link_set.has($(link).attr('href')) && ($(link).attr('href')).indexOf("https://medium") > -1){
	    				temp.push($(link).attr('href'))
	    				fs.appendFile("./async_output.csv",$(link).attr('href')+",")
	    				console.log($(link).attr('href'))
	    			}
	    			link_set.add($(link).attr('href'))
	    		}
	    		
	    	})
	  	} else {
	    	console.log("We’ve encountered an error: " + error);
	  	}
		callback()	  	
 
	})
    	
}


var queue = async.queue(crawl,5); // set concurrency here

// after all are processed
queue.drain = function() {

	var sub_links=Array.from(link_set)
	//console.log(sub_links)
	queue.push(sub_links)
};

fs.closeSync(fs.openSync("./async_output.csv", 'w'));

var start_url=['https://www.medium.com/']
queue.push(start_url);
