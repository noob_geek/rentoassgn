var request = require('request');
var Promise = require('bluebird');
var cheerio = require('cheerio');
var fs= require('fs')
var get = Promise.promisify(request.get);
var link_set = new Set // use a set to avoid Repetetion

var maxConnections = 5; //number of concurrent connections
var arrayOfLinks = [];

function scanPage(url) {

    return get(url).then((res) => {

        var body = res.body;
        var $ = cheerio.load(body);
        var links=$('a'); //get all links
                

        $(links).each(function(i,link){

            // add to the link_set if first occurence
            if(!link_set.has($(link).attr('href'))){
                console.log($(link).attr('href'))
                fs.appendFile("./promise_output.csv",$(link).attr('href')+",")
            }

            link_set.add($(link).attr('href'))
        })

        return Promise.map(link_set, scanPage, { concurrency: maxConnections });
    });

}

fs.closeSync(fs.openSync("./promise_output.csv", 'w'));

scanPage("https://www.medium.com/").then((res) => {
    writer.end()
});
